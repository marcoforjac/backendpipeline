import mongoose from 'mongoose'
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        minlength: 6
    },
    passwordsalt: { type: String },
    password: {
        type: String,
        required: true
    }
});

export default mongoose.model('User', userSchema);